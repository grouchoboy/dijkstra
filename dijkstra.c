#include "dijkstra.h"

void imprimirPadres(int padres[], int tam)
{
  printf("[");
  for (int i = 0; i < tam; i++)
  {
    printf(" %d ", padres[i]);
  }
  printf("]\n");
}

void dijkstra(struct grafo* g)
{
  queue_t cola;
  cola.numero_miembros = 0;
  cola.elementos = (queue_elemento_t *)
    malloc(sizeof(queue_elemento_t) * cola.numero_miembros);
  queue_elemento_t tmp;
  int distancia[g->v];
  int padre[g->v];
  int visitado[g->v];

#pragma omp parallel for
  for (int i = 0; i < g->v; i++)
  {
    distancia[i] = INT_MAX;
    padre[i] = -1;
    visitado[i] = 0;
  }
  distancia[0] = 0;
  tmp.u = 0;
  tmp.dist = distancia[0];
  insertarElemento(&cola, tmp);

  while (cola.numero_miembros != 0)
  {
    queue_elemento_t e = extraerMinimo(&cola);
    visitado[e.u] = 1;
    struct nodo* n = g->array[e.u].primero;

    while (n != NULL)
    {
      if (visitado[n->destino] == 0 &&
        distancia[n->destino] > distancia[e.u] + n->peso)
      {
        distancia[n->destino] = distancia[e.u] + n->peso;
        padre[n->destino] = e.u;
        queue_elemento_t elem;
        elem.u = n->destino;
        elem.dist = distancia[n->destino];
        insertarElemento(&cola, elem);
      }
      n = n->siguiente;
    }
  }
  //imprimirPadres(padre, g->v);
}
