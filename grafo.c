#include "grafo.h"

struct nodo* nuevoNodo(int destino)
{
  struct nodo* nuevoNodo = (struct nodo*) malloc(sizeof(struct nodo));
  nuevoNodo->destino = destino;
  nuevoNodo->siguiente = NULL;
  return nuevoNodo;
}

struct grafo* crearGrafo(int v)
{
  struct grafo* g = (struct grafo*) malloc(sizeof(struct grafo));
  g->v = v;

  g->array = (struct lista*) malloc(v * sizeof(struct lista));

  for (int i = 0; i < v; i++)
  {
    g->array[i].primero = NULL;
  }

  return g;
}

void addVertice(struct grafo* g, int src, int destino, int peso)
{
  struct nodo* n = nuevoNodo(destino);
  n->siguiente = g->array[src].primero;
  n->peso = peso;
  g->array[src].primero = n;

  n = nuevoNodo(src);
  n->siguiente = g->array[destino].primero;
  n->peso = peso;
  g->array[destino].primero = n;
}

void imprimirGrafo(struct grafo* g)
{
  for (int i = 0; i < g->v; i++)
  {
    struct nodo* l = g->array[i].primero;
    printf("\n Lista de adyacencia del vertice %d\n primero ", i);
    while (l)
    {
      printf("-(%d)-> %d", l->peso, l->destino);
      l = l->siguiente;
    }
    printf("\n");
  }
}

void liberarGrafo(struct grafo* g)
{
  if (g != NULL) {
    if (g->array != NULL) {
      for (int i = 0; i < g->v; i++)
      {
        struct nodo* n = g->array[i].primero;

        while (n != NULL) {
          struct nodo* tmp = n->siguiente;
          free(n);
          n = tmp;
        }
      }
    }
    free(g->array);
    free(g);
  }
}
