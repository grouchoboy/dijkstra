#ifndef _GRAFO_H
#define _GRAFO_H

#include <stdio.h>
#include <stdlib.h>

struct nodo
{
  int destino;
  int peso;
  struct nodo* siguiente;
};

struct lista
{
  struct nodo* primero;
};

struct grafo
{
  int v;
  struct lista* array;
};

struct nodo* nuevoNodo(int destino);
struct grafo* crearGrafo(int v);
void addVertice(struct grafo* g, int src, int destino, int peso);
void imprimirGrafo(struct grafo* g);
void liberarGrafo(struct grafo* g);

#endif
