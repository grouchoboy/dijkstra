#ifndef _QUEUE_H
#define _QUEUE_H
#include <stdlib.h>
#include <stdio.h>

typedef struct queue_elemento
{
  int u;
  int dist;
} queue_elemento_t;

typedef struct queue
{
  int numero_miembros;
  queue_elemento_t* elementos;
} queue_t;


void eliminarElemento(queue_t* p, int x);
queue_elemento_t extraerMinimo(queue_t* p);
void insertarElemento(queue_t* p, queue_elemento_t e);

#endif
