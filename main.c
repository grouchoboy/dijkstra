#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "dijkstra.h"

int main(int argc, char **argv) {

  int v;
  int multiplicador;
  if (argc == 3) {
    printf("Número de vértices: %s\n", argv[1]);
    v = atoi(argv[1]);
    printf("Número de vecinos por vértice: %s\n", argv[2]);
    multiplicador = atoi(argv[2]);
  } else {
    printf("Uso: ./main <número-vertices> <número-vecinos>\n");
    return -1;
  }

  srand(time(NULL));
  struct grafo *g = crearGrafo(v);

#pragma omp parallel for
  for (int i = 0; i < v; i++) {
#pragma omp parallel for
    for (int j = 0; j < multiplicador; j++) {
      addVertice(g, i, rand() % v, rand() % v);
    }
  }
  clock_t start = clock();

  for (int i = 0; i < 10; i++) {
    dijkstra(g);
  }
  free(g);
  clock_t end = clock();
  double tiempo = (double)(end - start);

  printf("Tiempo total: %f\n", tiempo / CLOCKS_PER_SEC);
  printf("Número de casos: 10\n");
  printf("Tiempo medio por caso: %f\n", (tiempo / CLOCKS_PER_SEC) / 10.0);
}
