#ifndef _DIJKSTRA_H
#define _DIJKSTRA_H

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <omp.h>

#include "grafo.h"
#include "queue.h"

void dijkstra(struct grafo* g);

#endif
