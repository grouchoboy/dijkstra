CC = gcc
STD = -std=c99
FLAGS = -fopenmp

main: main.o  grafo.o queue.o dijkstra.o
	$(CC) -o main main.o dijkstra.o grafo.o queue.o $(FLAGS) -O3

main.o: main.c  grafo.h queue.h dijkstra.h
	$(CC) -c -g -Wall $(STD) main.c $(FLAGS) -O3

dijkstra.o: dijkstra.c dijkstra.h grafo.h queue.h
	$(CC) -c -g -Wall $(STD) dijkstra.c $(FLAGS) -O3

queue.o: queue.h queue.c
	$(CC) -c -g -Wall $(STD) queue.c $(FLAGS) -O3

grafo.o: grafo.c grafo.h
	$(CC) -c -g -Wall $(STD) grafo.c $(FLAGS) -O3

clean:
	rm -f *.o main
