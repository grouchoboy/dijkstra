#include "queue.h"

void eliminarElemento(queue_t* p, int x)
{
  p->numero_miembros--;
  for (int i = 0; i < p->numero_miembros; i++)
  {
    p->elementos[i] = p->elementos[i+1];
  }

  if (p->numero_miembros == 0)
    p = NULL;
  else
  {
    p->elementos = (queue_elemento_t*)
      realloc(p->elementos, sizeof(queue_elemento_t) * p->numero_miembros);
  }
}

queue_elemento_t extraerMinimo(queue_t* p)
{
  int d_min = p->elementos[0].dist;
  int i_min = -1;
  queue_elemento_t e, e_min;
  for (int i = 0; i < p->numero_miembros; i++)
  {
    e = p->elementos[i];
    if (e.dist <= d_min) {
      e_min = e;
      i_min = i;
      d_min = e.dist;
    }
  }

  if (i_min == -1)
  {
    fprintf(stderr, "La cola no tiene elementos.\n");
  }

  eliminarElemento(p, i_min);
  return e_min;
}

void insertarElemento(queue_t* p, queue_elemento_t e)
{
  p->numero_miembros++;
  p->elementos = (queue_elemento_t*)
    realloc(p->elementos, sizeof(queue_elemento_t) * p->numero_miembros);
  p->elementos[p->numero_miembros - 1] = e;
}
